package com.javase.topics.polymorphism.test.breeds;

import com.javase.topics.polymorphism.test.Cat;
import com.javase.topics.polymorphism.test.HairsType;

public class PersianCat extends Cat{
    @Override
    public HairsType getHairsType() {
        return HairsType.FULL_HAIRS;
    }
}
