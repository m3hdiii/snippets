package com.javase.topics.polymorphism.test;

public interface Hairs {
    HairsType getHairsType();
}
