package com.javase.topics.polymorphism.test.computation;

import com.javase.topics.polymorphism.test.Cat;

public class ComputePrice {
    private static final Double BASE_PRICE = 100.0;
    private Cat[] cats;

    public ComputePrice(Cat... cats) {
        if (cats == null || cats.length == 0) {
            throw new IllegalArgumentException("You should pass at least one cat");
        }

        this.cats = cats;
    }


    public Double computeTotalPrice() {

        Double totalPrice = 0.0;

        for (Cat c : cats) {
            switch (c.getHairsType()) {
                case NO_HAIRS:
                    totalPrice += BASE_PRICE;
                    break;
                case SHORT_HAIRS:
                    totalPrice += raise(5.0);
                    break;
                case FULL_HAIRS:
                    totalPrice += raise(10.0);
                    break;
            }
        }

        return totalPrice;
    }

    private final Double raise(final Double raiseAmount) {
        if (raiseAmount <= 0 || raiseAmount > 100) {
            throw new IllegalArgumentException("Your raise value is wrong");
        }

        Double newRaisedPrice = ((raiseAmount / 100) * BASE_PRICE) + BASE_PRICE;
        return newRaisedPrice;
    }

    public final void computeIndividual() {
        final String format = "%s cat price is: %.2f";
        for (Cat c : cats) {
            String catName = c.getClass().getSimpleName();
            String text = "";
            switch (c.getHairsType()) {
                case NO_HAIRS:
                    text = String.format(format, catName, BASE_PRICE);
                    break;
                case SHORT_HAIRS:
                    text = String.format(format, catName, raise(5.0));
                    break;
                case FULL_HAIRS:
                    text = String.format(format, catName, raise(10.0));
                    break;
            }

            System.out.println(text);
        }
    }
}
