package com.javase.topics.polymorphism.test.run;

import com.javase.topics.polymorphism.test.Cat;
import com.javase.topics.polymorphism.test.breeds.AmericanShortHairCat;
import com.javase.topics.polymorphism.test.breeds.BurmeseCat;
import com.javase.topics.polymorphism.test.breeds.PersianCat;
import com.javase.topics.polymorphism.test.computation.ComputePrice;

public class Launcher {

    public static void main(String[] args) {
        Cat cat1 = new AmericanShortHairCat();
        Cat cat2 = new BurmeseCat();
        Cat cat3 = new PersianCat();

        ComputePrice cp = new ComputePrice(cat1, cat2, cat3);

        //Computations
        cp.computeIndividual();
        System.out.println("==================================");
        Double total = cp.computeTotalPrice();
        System.out.println(String.format("Total price is: %.2f", total));

    }
}
