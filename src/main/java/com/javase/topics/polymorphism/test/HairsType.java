package com.javase.topics.polymorphism.test;

public enum HairsType {
    FULL_HAIRS, SHORT_HAIRS, NO_HAIRS
}
